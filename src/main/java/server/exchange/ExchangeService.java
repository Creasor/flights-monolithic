package server.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.common.model.ExchangeRate;

/**
 * This class defines implementation methods that are forwarded from
 * the {@link FlightService}.
 *
 * This class is annotated as a Spring {@code Service}, which enables
 * the autodetection of dependent implementation classes via classpath
 * scanning.
 */
@Service
public class ExchangeService {
    /**
     * An auto-wired field that connects the ExchangeService to the
     * ExchangeRepository.
     */
    @Autowired
    ExchangeRepository repository;

    /**
     * @return The {@link ExchangeRate} for the given {@code
     * fromCurrency} and {@code toCurrency}
     */
    public ExchangeRate getRate(String fromCurrency, String toCurrency) {
        return repository
            // Forward to the repository.
            .findByFromCurrencyAndToCurrency(fromCurrency, toCurrency);
    }
}
