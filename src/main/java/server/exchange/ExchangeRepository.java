package server.exchange;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import server.common.model.ExchangeRate;

/**
 * A persistent repository that contains information about {@link
 * ExchangeRate} objects.  
 * 
 * The {@code Repository} annotation indicates that this class
 * provides the mechanism for storage, retrieval, search, update and
 * delete operation on {@link ExchangeRate} objects.
 */
@Repository
public interface ExchangeRepository
       extends JpaRepository<ExchangeRate, Long> {
    /**
     * @return The {@link ExchangeRate} for the given {@code
     * fromCurrency} and {@code toCurrency}.
     */
    ExchangeRate findByFromCurrencyAndToCurrency
        (@Param("fromCurrency") String fromCurrency,
         @Param("toCurrency") String toCurrency);
}
