package server.common;

import server.flight.FlightController;

/**
 * Static class used to centralize all constants used by the
 * monolithic implementation of the Flight Listing App (FLApp).
 */
public class Constants {
    /**
     * Endpoint names for methods defined in the FLApp {@link
     * FlightController}.
     */
    public static class EndPoint {
        public static final String RATE = "rate";
        public static final String AIRPORTS = "airports";
        public static final String BEST_PRICE = "best-price";
        public static final String FLIGHTS = "flights";
        public static final String FLIGHT_DATES = "dates";
    }
}
