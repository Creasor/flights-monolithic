package server.common.model;

import java.time.LocalDate;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;

/**
 * This "Plain Old Java Object" (POJO) class defines a request for a
 * flight.
 *
 * The {@code @Value} annotation assigns default values to variables.
 *
 * The {@code @RequiredArgsConstructor} generates a constructor with 1
 * parameter for each field that requires special handling.  
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter.
 *
 * The {@code @Builder} annotation automatically creates a static
 * builder factory method for this class that can be used as follows:
 *
 * FlightRequest flightRequest = FlightRequest
 *   .builder()
 *   .departureAirport("JFK")
 *   .arrivalAirport("BWI")
 *   ...
 *   .build();
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Builder
public class FlightRequest {
    /**
     * The name of the departure airport.
     */
    String departureAirport;

    /**
     * The name of the arrival airport.
     */
    String arrivalAirport;

    /**
     * The date of the departure.
     */
    LocalDate departureDate;

    /**
     * The number of passengers who want to travel.
     */
    int passengers;

    /**
     * The currency of the price.
     */
    String currency;
}
