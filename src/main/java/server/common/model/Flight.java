package server.common.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;

/**
 * This "Plain Old Java Object" (POJO) class defines a response for a
 * flight, which is returned by various objects to indicate which
 * flights match a {@link FlightRequest}.
 * 
 * The {@code @Value} annotation assigns default values to variables.
 *
 * The {@code @RequiredArgsConstructor} generates a constructor with 1
 * parameter for each field that requires special handling.
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter.
 *
 * The {@code @Builder} annotation automatically creates a static
 * builder factory method for this class that can be used as follows:
 *
 * Flight flight = Flight
 *   .builder()
 *   .departureAirport("JFK")
 *   .arrivalAirport("BWI")
 *   ...
 *   .build();
 * 
 * The {@code @Entity} annotation specifies that this class is an
 * entity and is mapped to a database table. 
 *
 * The {@code @With} annotation generates a method that constructs a
 * clone of the object, but with a new value for this one field.
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Builder
@Entity
@With
public class Flight {
    /**
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link Flight} object.  The
     * {@code @GeneratedValue} annotation configures the specified
     * field to auto-increment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The name of the departure airport.
     */
    String departureAirport;

    /**
     * The date of the departure.
     */
    LocalDate departureDate;

    /**
     * The time of the departure.
     */
    LocalTime departureTime;

    /**
     * The name of the arrival airport.
     */
    String arrivalAirport;

    /**
     * The date of the arrival.
     */
    LocalDate arrivalDate;

    /**
     * The time of the arrival.
     */
    LocalTime arrivalTime;

    /**
     * The distance in kilometers from the departure to the arrival
     * airports.
     */
    int kilometers;

    /**
     * The price of the flight.  The {@code @Column} annotation
     * customizes the mapping between the entity attribute and the
     * database column, which in this case defines the scale and
     * precision of a decimal price.
     */
    @Column(precision = 10, scale = 2)
    double price;

    /**
     * The currency of the price.  The {@code @Column} annotation
     * customizes the mapping between the entity attribute and the
     * database column, which in this case defines the length of
     * String-valued database column to be 3 characters.
     */
    @Column(length = 3)
    String currency;

    /**
     * The airline code.  The {@code @Column} annotation customizes
     * the mapping between the entity attribute and the database
     * column, which in this case defines the length of String-valued
     * database column to be 3 characters.
     */
    @Column(length = 3)
    String airlineCode;

    /**
     * This method creates sample data for testing.
     *
     * @return SQL insert header string
     */
    public static String insertIntoTableHeader() {
        return "insert into FLIGHT (\n"
            + "\tid,\n"
            + "\tdeparture_airport,\n"
            + "\tdeparture_date,\n"
            + "\tdeparture_time,\n"
            + "\tarrival_airport,\n"
            + "\tarrival_date,\n"
            + "\tarrival_time,\n"
            + "\tkilometers,\n"
            + "\tprice,\n"
            + "\tcurrency,\n"
            + "\tairline_code\n"
            + "\tcapacity\n"
            + ") values\n";
    }

    /**
     * This method creates sample data for testing.
     *
     * @return SQL insert value entry string
     */
    public String insertTableValueEntry() {
        return "(default" + ", '"
            + departureAirport + "'" + ", '"
            + departureDate + "'" + ", '"
            + departureTime + "'" + ", '"
            + arrivalAirport + "'" + ", '"
            + arrivalDate + "'" + ", '"
            + arrivalTime + "'" + ", "
            + kilometers + ", "
            + price + ", '"
            + currency + "'" + ", '"
            + airlineCode + "'" + ')';
    }

    /**
     * This method creates an SQL insert table definition that can be
     * used to pre-load the FLIGHT database table.
     *
     * @param flights A {@link List} of {@link Flight} objects
     * @return A complete table insert definition
     */
    public static String toSqlInsertString(List<Flight> flights) {
        StringBuilder builder =
            new StringBuilder(Flight.insertIntoTableHeader());

        for (int i = 0; i < flights.size(); i++) {
            builder.append("\t");
            builder.append(flights.get(i).insertTableValueEntry());
            builder.append(i < flights.size() - 1 ? ",\n" : ";");
        }

        return builder.toString();
    }
}
