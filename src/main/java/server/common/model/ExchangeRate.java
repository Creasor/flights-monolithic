package server.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.*;

/**
 * This "Plain Old Java Object" (POJO) class keeps track of currency
 * to convert from and the currency to convert to.
 *
 * The {@code @Data} annotation generates all the boilerplate that is
 * normally associated with simple POJOs and beans: getters for all
 * fields, setters for all non-final fields, and appropriate toString,
 * equals and hashCode implementations that involve the fields of the
 * class, and a constructor that initializes all final fields.
 *
 * The {@code @RequiredArgsConstructor} generates a constructor with 1
 * parameter for each field that requires special handling.  
 *
 * The {@code Entity} annotation specifies that this class is an
 * entity and is mapped to a database table.
 */
@Data
@RequiredArgsConstructor
@Entity
public class ExchangeRate {
    /**
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link ExchangeRate} object.  The
     * {@code @GeneratedValue} annotation configures the specified
     * field to auto-increment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The name of the currency to convert from.
     */
    String fromCurrency;

    /**
     * The name of the currency to convert to.
     */
    String toCurrency;

    /**
     * The exchange rate for the "from" currency to the "to" currency.
     */
    double exchangeRate;

    /**
     * This constructor initializes the fields from the params.
     */
    public ExchangeRate(String fromCurrency,
                        String toCurrency,
                        double exchangeRate) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.exchangeRate = exchangeRate;
    }
}
