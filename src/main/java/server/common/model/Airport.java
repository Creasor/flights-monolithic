package server.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;

/**
 * This "Plain Old Java Object" (POJO) class defines information about
 * an airport.
 *
 * The {@code @Value} annotation assigns default values to variables.
 *
 * The {@code @RequiredArgsConstructor} generates a constructor with 1
 * parameter for each field that requires special handling.
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter.* The @Entity annotation specifies that this class is an
 * entity and is mapped to a database table.
 *
 * The {@code @Builder} annotation produces complex builder APIs for
 * your classes, e.g., it automatically produces code required to have
 * a class be instantiable like this:
 *
 * Airport
 *   .builder()
 *   .airportCode("BWI")
 *   .airportName("Baltimore, MD")
 *   .build();
 *
 * The {@code Entity} annotation specifies that this class is an
 * entity and is mapped to a database table.
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Builder
@Entity
public class Airport {
    /**
     * The three-letter airport code is the unique primary key.
     *
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link Airport} object.
     *
     * The {@code @Column} annotation customizes the mapping between
     * the entity attribute and the database column, which in this
     * case defines the length of String-valued database column to be
     * 3 characters.
     */
    @Id
    @Column(length = 3)
    String airportCode;

    /**
     * The airport name (e.g., city, state, and country).
     */
    String airportName;
}
