package server.airport;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import server.common.model.Airport;

/**
 * A persistent repository that contains information about {@link
 * Airport} objects.
 *
 * The {@code Repository} annotation indicates that this class
 * provides the mechanism for storage, retrieval, search, update and
 * delete operation on {@link Airport} objects.
 */
@Repository
public interface AirportRepository
       extends JpaRepository<Airport, String> {
    /**
     * There's an implicit method findAll() inherited from
     * JpaRepository.
     */
}
