package server.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import server.common.model.Airport;

/**
 * This class defines implementation methods that are forwarded from
 * the {@link server.flight.FlightController}.
 *
 * This class is annotated as a Spring {@code Service}, which enables
 * the autodetection of dependent implementation classes via classpath
 * scanning.
 */
@Service
public class AirportService {
    /**
     * An auto-wired field that connects the {@link AirportService} to
     * the {@link AirportRepository}.
     */
    @Autowired
    private AirportRepository repository;

    /**
     * @return A {@link List} of all available {@link Airport}
     * objects
     */
    public List<Airport> getAirports() {
        return repository
            // Forward to the persistent repository.
            .findAll();
    }
}
