package server.airline;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import server.common.model.Flight;

/**
 * A persistent repository that contains information about Airline
 * {@link Flight} objects.
 *
 * The {@code Repository} annotation indicates that this class
 * provides the mechanism for storage, retrieval, search, update and
 * delete operation on {@link Flight} objects.
 */
@Repository
public interface AirlineRepository
       extends JpaRepository<Flight, Long> {
    /**
     * Find all flights that match the passed airports and departure
     * date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A List of matching {@link Flight} objects.
     */
    List<Flight> findByDepartureAirportAndDepartureDateAndArrivalAirport
        (@Param("departureAirport") String departureAirport,
         @Param("departureDate") LocalDate departureDate,
         @Param("arrivalAirport") String arrivalAirport
    );

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all matching dates
     */
    @Query("select DISTINCT departureDate from #{#entityName} " +
           "where departureAirport = :departureAirport " +
           "and arrivalAirport = :arrivalAirport " +
           "order by departureDate asc")
    List<LocalDate> findDepartureDates
        (@Param("departureAirport") String departureAirport,
         @Param("arrivalAirport") String arrivalAirport
    );
}
