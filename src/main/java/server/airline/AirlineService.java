package server.airline;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import server.common.model.Flight;

/**
 * This class defines implementation methods that are forwarded from
 * the {@link server.flight.FlightController}.
 *
 * This class is annotated as a Spring {@code Service}, which enables
 * the autodetection of dependent implementation classes via classpath
 * scanning.
 */
@Service
public class AirlineService {
    /**
     * An auto-wired field that connects the AirlineService to the
     * AirlineRepository that stores airline information persistently.
     */
    @Autowired
    AirlineRepository repository;

    /**
     * Find all flights that match the passed airports and departure
     * date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of matching {@link Flight} objects in
     * the desired currency
     */
    public List<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findByDepartureAirportAndDepartureDateAndArrivalAirport(departureAirport,
                                                                     departureDate,
                                                                     arrivalAirport);
    }

    /**
     * Find the best priced flights that match the passed airports and
     * departure date.
     *
     * @return All {@link Flight} objects that share the lowest price
     * or null if no matching flights were found
     */
    public List<Flight> findBestPrice(String departureAirport,
                                      LocalDate departureDate,
                                      String arrivalAirport) {
        // Find the minimum price for the all matching flights.
        List<Flight> flights =
            findFlights(departureAirport, departureDate, arrivalAirport);

        Double min = flights
            // Convert to stream.
            .stream()

            // Find the minimum price
            .min(Comparator.comparingDouble(Flight::getPrice))

            // Extract price field.
            .map(Flight::getPrice)

            // Return null if no matching flights were found.
            .orElse(null);

        // Sanity check.
        if (min == null)
            return null;
        else
            // Find and return all flights with this minimum price.
            return flights
                // Convert the List into a Stream.
                .stream()

                // Only allow flights that match the minimum price.
                .filter(flight -> flight.getPrice() == min)

                // Collect the results into a List.
                .collect(Collectors.toList());
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A List of all matching departure dates
     */
    public List<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}
