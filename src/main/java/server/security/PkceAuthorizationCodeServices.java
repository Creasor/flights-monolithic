/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2019 https://github.com/AnarSultanov/examples/tree/master/spring-boot-oauth2-pkce
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package server.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides services for issuing and storing authorization codes used
 * by the in-memory Proof Key for Code Exchange (PKCE) authorization
 * server.
 */
public class PkceAuthorizationCodeServices implements AuthorizationCodeServices {
    private final RandomValueStringGenerator generator = new RandomValueStringGenerator();
    private final Map<String, PkceProtectedAuthentication> authorizationCodeStore =
        new ConcurrentHashMap<>();

    private final ClientDetailsService clientDetailsService;
    private final PasswordEncoder passwordEncoder;

    public PkceAuthorizationCodeServices(ClientDetailsService clientDetailsService,
                                         PasswordEncoder passwordEncoder) {
        this.clientDetailsService = clientDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Creates and stores a temporary authorization code that will be
     * eventually returned to the client in response to response to an
     * "/oauth/authorize" end-point request.
     *
     * @param authentication {@link OAuth2Authentication} token granter.
     * @return The saved authentication code.
     */
    @Override
    public String createAuthorizationCode(OAuth2Authentication authentication) {
        PkceProtectedAuthentication protectedAuthentication = getProtectedAuthentication(authentication);
        String code = generator.generate();
        authorizationCodeStore.put(code, protectedAuthentication);
        return code;
    }

    /**
     * Allows access to the {@link PkceProtectedAuthentication}
     * instance if either the request is from a public client and
     * provides a valid code_challenge, or the request is not form a
     * public client and has instead provided a client secret.
     *
     * @param authentication
     * @return
     */
    private PkceProtectedAuthentication getProtectedAuthentication(OAuth2Authentication authentication) {
        Map<String, String> requestParameters = authentication.getOAuth2Request().getRequestParameters();

        // Public clients that are unable to provide a client secret
        // must use a code challenge to prove their identity.
        if (isPublicClient(requestParameters.get("client_id")) 
            && !requestParameters.containsKey("code_challenge")) {
            throw new InvalidRequestException("Code challenge required.");
        }

        // If a code challenge is required, then construct return a
        // protection wrapper class instance that will only provide
        // allow access to an token authentication instance if a
        // matching code verifier is provided in token requests.
        if (requestParameters.containsKey("code_challenge")) {
            String codeChallenge = requestParameters.get("code_challenge");
            CodeChallengeMethod codeChallengeMethod = getCodeChallengeMethod(requestParameters);
            return new PkceProtectedAuthentication(codeChallenge, codeChallengeMethod, authentication);
        }

        // A code challenge is not required so return a protection
        // wrapper class instance that will not require the client to
        // provide a code verification making a token request.
        return new PkceProtectedAuthentication(authentication);
    }

    /**
     * Returns a {@link CodeChallengeMethod} enumerated type that
     * matches the authentication request's "code_challenge_method"
     * parameter.
     *
     * @param requestParameters Authentication request parameters.
     * @return The {@link CodeChallengeMethod} enumerated type.
     */
    private CodeChallengeMethod getCodeChallengeMethod(Map<String, String> requestParameters) {
        try {
            return Optional.ofNullable(requestParameters.get("code_challenge_method"))
                    .map(String::toUpperCase)
                    .map(CodeChallengeMethod::valueOf)
                    .orElse(CodeChallengeMethod.PLAIN);
        } catch (IllegalArgumentException e) {
            throw new InvalidRequestException("Transform algorithm not supported");
        }
    }

    /**
     * Determines if the client is a public client, i.e., has not
     * provided a client secret.
     *
     * @param clientId The client ID specified in the authorization request.
     * @return True if the client is public and has no associated client secret, false if
     * the client has an associated secret.
     */
    private boolean isPublicClient(String clientId) {
        String clientSecret = clientDetailsService.loadClientByClientId(clientId).getClientSecret();
        return clientSecret == null || passwordEncoder.matches("", clientSecret);
    }

    /**
     * Retrieves the cached {@link PkceProtectedAuthentication}
     * instance that matches the passed authentication code and then
     * requests an authentication token instance passing in the code
     * verifier to ensure a proper match with a previously stored code
     * challenge.
     *
     * @param code The authentication token created by the original authentication request.
     * @param verifier A code verifier used to ensure that that the original authentication
     *                 request client is the same client that is now requesting a token.
     * @return A valid token authentication instance.
     */
    public OAuth2Authentication consumeAuthorizationCodeAndCodeVerifier(String code, String verifier) {
        return authorizationCodeStore.get(code).getAuthentication(verifier);
    }


    /**
     * Not supported.
     */
    @Override
    public OAuth2Authentication consumeAuthorizationCode(String code) {
        throw new UnsupportedOperationException();
    }
}
