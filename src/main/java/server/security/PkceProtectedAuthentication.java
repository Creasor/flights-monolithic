/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2019 https://github.com/AnarSultanov/examples/tree/master/spring-boot-oauth2-pkce
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package server.security;

import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * This class protects access to {@link OAuth2Authentication} instance
 * by requiring the client to provide a valid code verifier that
 * matches a previously client supplied code challenge. A match
 * failure will prevent Cross-Site Request Forgery (CSRF) attacks on
 * this OAuth server.
 */
public class PkceProtectedAuthentication {
    /**
     * Code challenge String supplied by client.
     */
    private final String codeChallenge;

    /**
     * The method used to transform the code verification String to
     * attempt to match with the code challenge String.
     */
    private final CodeChallengeMethod codeChallengeMethod;

    /**
     * The protected {@link OAuth2Authentication} object.
     */
    private final OAuth2Authentication authentication;

    /**
     * Constructs a instance where the client can be identified by a
     * client secret instead of a code challenge.
     *
     * @param authentication The {@link OAuth2Authentication} instance
     *                       that is protected by this PKCE authenticaion
     *                       wrapper.
     */
    public PkceProtectedAuthentication(OAuth2Authentication authentication) {
        this.codeChallenge = null;
        this.codeChallengeMethod = CodeChallengeMethod.NONE;
        this.authentication = authentication;
    }

    /**
     * Constructor initializes the fields.
     */
    public PkceProtectedAuthentication(String codeChallenge,
                                       CodeChallengeMethod codeChallengeMethod,
                                       OAuth2Authentication authentication) {
        this.codeChallenge = codeChallenge;
        this.codeChallengeMethod = codeChallengeMethod;
        this.authentication = authentication;
    }

    /**
     * Called to access to {@link OAuth2Authentication} which can then
     * be used to grant access tokens. Access tokens will be granted
     * only if the client supplied code verification String matches
     * the previously client supplied code challenge String (after
     * applying the appropriate transform method).
     *
     * @param codeVerifier A code verification String sent by the client
     *                     that is required to match the code challenge.
     * @return The protected {@link OAuth2Authentication} instance that can
     * be then be used to grant access tokens.
     */
    public OAuth2Authentication getAuthentication(String codeVerifier) {
        if (codeChallengeMethod == CodeChallengeMethod.NONE) {
            return authentication;
        } else if (codeChallengeMethod.transform(codeVerifier).equals(codeChallenge)) {
            return authentication;
        } else {
            throw new InvalidGrantException("Invalid code verifier.");
        }
    }
}
