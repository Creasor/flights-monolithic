/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2019 https://github.com/AnarSultanov/examples/tree/master/spring-boot-oauth2-pkce
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package server.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.common.exceptions.RedirectMismatchException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.HashMap;
import java.util.Map;

/**
 * A Custom in-memory Proof Key for Code Exchange (PKCE) token granter
 * that validates all token request paramters before returning a an
 * {@link OAuth2Authentication} token instance.
 */
public class PkceAuthorizationCodeTokenGranter extends AuthorizationCodeTokenGranter {
    /**
     * Custom PKCE authentication services injected by {@link
     * AuthServerConfig}.
     */
    private final PkceAuthorizationCodeServices authorizationCodeServices;

    /**
     * Constructor initializes the fields.
     */
    public PkceAuthorizationCodeTokenGranter(AuthorizationServerTokenServices tokenServices,
                                             PkceAuthorizationCodeServices authorizationCodeServices,
                                             ClientDetailsService clientDetailsService,
                                             OAuth2RequestFactory requestFactory) {
        super(tokenServices, authorizationCodeServices, clientDetailsService, requestFactory);
        this.authorizationCodeServices = authorizationCodeServices;
    }

    /**
     * Called to complete the PKCE authentication flow by granting an
     * access token (may also include a refresh token).
     *
     * @param client Client detail
     * @param tokenRequest A new {@link TokenRequest} to process.
     * @return A new {@link OAuth2Authentication} token instance.
     */
    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client,
                                                           TokenRequest tokenRequest) {
        // Extract all request parameters from token request.
        Map<String, String> parameters = tokenRequest.getRequestParameters();

        // Request must contain an previously supplied authorization code.
        String authorizationCode = parameters.get("code");
        if (authorizationCode == null) {
            throw new InvalidRequestException("An authorization code must be supplied.");
        }

        // Request must contain a code verifier String if the CodeChallengeMethod
        // is either S256 or PLAIN and that verification String must match a previously
        // client supplied code challenge String.
        String codeVerifier = parameters.getOrDefault("code_verifier", "");
        OAuth2Authentication storedAuth = authorizationCodeServices
            .consumeAuthorizationCodeAndCodeVerifier(authorizationCode, codeVerifier);
        if (storedAuth == null) {
            throw new InvalidGrantException("Invalid authorization code: " + authorizationCode);
        }

        // Access the pending authorization request to check for
        // parameter matches with this token rquest.
        OAuth2Request pendingOAuth2Request = storedAuth.getOAuth2Request();

        // Pending authorization request and this token request must have
        // the same redirect uri.
        String pendingRedirectUri = pendingOAuth2Request.getRedirectUri();
        String redirectUri = parameters.get("redirect_uri");
        if ((redirectUri != null || pendingRedirectUri != null)
                && !pendingOAuth2Request.getRedirectUri().equals(redirectUri)) {
            throw new RedirectMismatchException("Redirect URI mismatch.");
        }

        // Pending authorization request and this token request must have
        // the same client ID.
        String pendingClientId = pendingOAuth2Request.getClientId();
        String clientId = tokenRequest.getClientId();
        if ((pendingClientId != null || clientId != null) && !clientId.equals(pendingClientId)) {
            throw new InvalidClientException("Client ID mismatch");
        }

        // All security checks have passed so complete the request by
        // building and returning a new OAuth2Authentication token
        // instance.
        Map<String, String> combinedParameters = new HashMap<>(pendingOAuth2Request.getRequestParameters());
        combinedParameters.putAll(parameters);
        OAuth2Request finalStoredOAuth2Request = pendingOAuth2Request.createOAuth2Request(combinedParameters);
        Authentication userAuth = storedAuth.getUserAuthentication();
        return new OAuth2Authentication(finalStoredOAuth2Request, userAuth);
    }
}
