/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2019 https://github.com/AnarSultanov/examples/tree/master/spring-boot-oauth2-pkce
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package server.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * Configuration setup for an in-memory Proof Key for Code Exchange
 * (PKCE) authorization server.
 */
@Configuration
@EnableResourceServer
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * Dynamically resolve user name from the application.properties
     * resource file.
     */
    @Value("${users.user1.name}")
    public String testUserName;

    /**
     * Dynamically resolve user password from the
     * application.properties resource file.
     */
    @Value("${users.user1.password}")
    public String testUserPassword;

    /**
     * This bean ensures that all passwords will be automatically
     * encoded.
     *
     * @return A delegating password encoder with all default properties.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    /**
     * This bean exposes the default authentication manager bean.
     *
     * @return The authentication manager bean.
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Configures the HttpSecurity that allows configuring web based
     * security for specific http request.
     *
     * @param http The {@link HttpSecurity} object to configure.
     */
    protected void configure(HttpSecurity http) throws Exception {
        http
                // Declare security endpoints (/logout is available by default)
                .requestMatchers().antMatchers("/login", "/oauth/authorize")

                .and()

                // Require all other endpoints to have full authentication.

                .authorizeRequests().anyRequest().authenticated()

                .and()

                // Automatically generate and redirect to a login page
                // that is available to unauthenticated users.
                .formLogin().permitAll();
    }

    /**
     * Called by WebSecurityConfigureAdapter to setup an {@link
     * AuthenticationManager}.  In this example, we setup an in memory
     * authentication server.
     *
     * @param auth The {@link AuthenticationManagerBuilder} to customize.
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                // Use in memory authentication
                .inMemoryAuthentication()


                // Set a test user.
                .withUser(testUserName)

                // Set and encode the test user password.
                .password(passwordEncoder().encode(testUserPassword))

                // Test user has a single "USER" role.
                .roles("USER");
    }

    /**
     * This bean exposes the default {@link UserDetailsService} bean.
     *
     * @return The default {@link UserDetailsService} bean.
     */
    @Override
    @Bean("userDetailsService")
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }
}
