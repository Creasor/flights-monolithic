/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2019 https://github.com/AnarSultanov/examples/tree/master/spring-boot-oauth2-pkce
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {
    /**
     * Dynamically resolve client id from the application.properties
     * resource file.
     */
    @Value("${oauth.client.id}")
    private String clientId;

    /**
     * Dynamically resolve redirect uri from the
     * application.properties resource file.
     */
    @Value("${oauth.client.redirectUri}")
    private String clientRedirectUri;

    /**
     * Inject UserDetailsService bean.
     */
    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;

    /**
     * Constructor initializes the fields.
     */
    public AuthServerConfig(PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
    }

    /**
     * Configure authorization server to use Form authentication.
     *
     * @param security Instance to configure/update.
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.allowFormAuthenticationForClients();
    }

    /**
     * Configures the properties and enhanced functionality of the
     * Authorization Server endpoints to use a full PKCE
     * authentication flow.
     *
     * @param endpoints Authorization server endpoints to configure.
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
            // Set PKCE authorization code services.
            .authorizationCodeServices
            (new PkceAuthorizationCodeServices(endpoints.getClientDetailsService(),
                                               passwordEncoder))

            // Set PKCE token granter.
            .tokenGranter(tokenGranter(endpoints))

            // Set the default UserDetailsService.
            .userDetailsService(userDetailsService);
    }

    /**
     * Builds and returns composite token granter.
     *
     * @param endpoints Authorization server endpoints to configure.
     * @return A composite {@link TokenGranter} that supports a PKCE
     * authentication requiring a client username and password and
     * providing token refresh support.
     */
    private TokenGranter tokenGranter(final AuthorizationServerEndpointsConfigurer endpoints) {
        List<TokenGranter> granters = new ArrayList<>();

        AuthorizationServerTokenServices tokenServices = endpoints.getTokenServices();
        AuthorizationCodeServices authorizationCodeServices = endpoints.getAuthorizationCodeServices();
        ClientDetailsService clientDetailsService = endpoints.getClientDetailsService();
        OAuth2RequestFactory requestFactory = endpoints.getOAuth2RequestFactory();

        granters.add(new RefreshTokenGranter(tokenServices, clientDetailsService, requestFactory));
        granters.add(new ImplicitTokenGranter(tokenServices, clientDetailsService, requestFactory));
        granters.add(new ClientCredentialsTokenGranter(tokenServices, clientDetailsService, requestFactory));
        granters.add(new ResourceOwnerPasswordTokenGranter(authenticationManager,
                                                           tokenServices,
                                                           clientDetailsService,
                                                           requestFactory));
        granters.add(new PkceAuthorizationCodeTokenGranter(tokenServices,
                                                           ((PkceAuthorizationCodeServices) authorizationCodeServices),
                                                           clientDetailsService, requestFactory));

        return new CompositeTokenGranter(granters);
    }

    /**
     * Configures a OAuth 2 PKCE in memory authentication server that includes
     * token refresh support.
     *
     * @param clients {@link ClientDetailsServiceConfigurer} instance to configure.
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
            // Use an in memory server.
            .inMemory()

            // Set a fixed client id.
            .withClient(clientId)

            // Only support a PCKE public client (no client secret
            // provided).
            .secret("{noop}")

            // Set the redirect uri used the authentication and token
            // requests.
            .redirectUris(clientRedirectUri)

            // Tokens are granted using authorization/token requests
            // or in response to a token refresh request.
            .authorizedGrantTypes("authorization_code", "refresh_token")

            // Only allow read access, but also allow a refresh tokens
            // to never expire ("offline_access").
            .scopes("read", "offline_access")

            // Automatically approve all valid requests.
            .autoApprove(true);
    }
}
