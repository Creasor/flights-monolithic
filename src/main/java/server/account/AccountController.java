package server.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET and POST requests via object-oriented programming.
 * These requests are mapped to methods that synchronously manage user
 * account information.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * The {@code RequestMapping} annotation indicates how to map web
 * requests onto methods in request-handling classes by designating a
 * portion of the relevant path (in this case "/account").
 *
 * Spring uses the {@code @GetMapping} and {@code PostMapping}
 * annotations to map HTTP GET and POST requests onto methods in the
 * {@code AccountController}, respectively.  These requests invoked
 * from any HTTP web client (e.g., a web browser or Android app) or
 * command-line utility (e.g., Curl or Postman).
 */
@RestController
@RequestMapping("/account")
public class AccountController {
    /**
     * This auto-wired field connects the AccountController to the
     * PasswordEncoder.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Returns the information about the currently logged in user's
     * account.
     *
     * @param authentication The {@link Authentication} token
     * @return Information about the currently logged in user's
     * account
     */
    @GetMapping
    public UserDetails getCurrentUser(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return userDetails;
    }
}
