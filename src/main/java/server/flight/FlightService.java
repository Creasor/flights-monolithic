package server.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.airline.AirlineService;
import server.airport.AirportService;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;
import server.exchange.ExchangeService;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * This class defines implementation methods that are forwarded from
 * the {@link FlightController}.
 *
 * It is annotated as a Spring {@code Service}, which enables the
 * auto-detection and wiring of dependent implementation classes
 * via classpath scanning.
 */
@Service
public class FlightService {
    /**
     * An auto-wired field that connects the FlightService to the
     * AirportService.
     */
    @Autowired
    AirportService airportService;

    /**
     * An auto-wired field that connects the FlightService to the
     * AirlineService.
     */
    @Autowired
    AirlineService airlineService;

    /**
     * An auto-wired field that connects the FlightService to the
     * ExchangeService.
     */
    @Autowired
    ExchangeService exchangeService;

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @param toCurrency The desired currency
     * @return A {@link List} of matching {@link Flight} objects in
     * the desired currency
     */
    public List<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport,
                                    String toCurrency) {
        Map<String, List<Flight>> flightsMap = airlineService
            // Return a List of all flights that match the params.
            .findFlights(departureAirport,
                         departureDate,
                         arrivalAirport)
            
            // Convert the List into a Stream.
            .stream()

            // Create a Map that groups flights (values) according to their
            // currency (keys).
            .collect(Collectors.groupingBy(Flight::getCurrency, toList()));

        return flightsMap
            // Convert the Map into a Set of Map.Entry objects.
            .entrySet()

            // Convert the Set into a Stream.
            .stream()

            // Convert the fromCurrency into the toCurrency.
            .flatMap(entry ->
                     tryToConvertCurrency(entry.getKey(),
                                          toCurrency,
                                          entry.getValue().stream()))

            // Return a List of Flight objects.
            .collect(toList());
    }

    /**
     * Use the {@link ExchangeService} to convert the prices of all
     * flights from the {@code fromCurrency} to the {@code
     * toCurrency}.
     *
     * @param fromCurrency The currency to convert from
     * @param toCurrency   The currency to convert to
     * @param flightStream A Stream of Flight objects
     * @return A {@link Stream} of Flight objects that have been converted from
     *         the {@code fromCurrency} to the {@code toCurrency}
     */
    private Stream<Flight> tryToConvertCurrency(String fromCurrency,
                                                String toCurrency,
                                                Stream<Flight> flightStream) {
        // Only call ExchangeService if fromCurrency and toCurrency
        // are different!
        if (!fromCurrency.equals(toCurrency)) {
            // Compute the exchange rate from fromCurrency to
            // toCurrency.
            ExchangeRate rate =
                exchangeService.getRate(fromCurrency, toCurrency);

            // Update the flightStream with the latest currency
            // exchange rate.
            flightStream = flightStream
                // Multiply/update each price by the exchange rate.
                .map(flight -> flight
                     .withPrice(flight.getPrice() * rate.getExchangeRate()));
        }

        // Return the flightStream, which may or may not have been
        // updated.
        return flightStream;
    }

    /**
     * Find the best priced flights that match the passed airports and
     * departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param currency The currency
     * @return A {@link List} of all {@link Flight} objects that share the
     * lowest price
     */
    public List<Flight> findBestPrice(String departureAirport,
                                      LocalDate departureDate,
                                      String arrivalAirport,
                                      String currency) {
        return airlineService
            // Forward to the AirlineService.
            .findBestPrice(departureAirport,
                           departureDate,
                           arrivalAirport);
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all matching dates
     */
    public List<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return airlineService
            // Forward to the AirlineService.
            .findDepartureDates(departureAirport, arrivalAirport);
    }

    /**
     * Finds all known airports.
     *
     * @return A {@link List} of {@code Airport} objects
     */
    public List<Airport> getAirports() {
        return airportService
            // Forward to the AirportService.
            .getAirports();
    }

    /**
     * Finds the latest exchange rate that matches the {@code
     * currencyConversion} fields.
     *
     * @param fromCurrency The 3 letter currency to convert from.
     * @param toCurrency   The 3 letter currency to convert to.
     * @return An {@link ExchangeRate} object that contains the passed
     * parameters along with the latest exchange rate
     */
    public ExchangeRate getRate(String fromCurrency, String toCurrency) {
        return exchangeService
            // Forward to the ExchangeService.
            .getRate(fromCurrency, toCurrency);
    }
}
