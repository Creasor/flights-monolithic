package server.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

import server.common.model.Airport;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static server.common.Constants.EndPoint.AIRPORTS;
import static server.common.Constants.EndPoint.BEST_PRICE;
import static server.common.Constants.EndPoint.FLIGHTS;
import static server.common.Constants.EndPoint.FLIGHT_DATES;

/**
 * This Spring controller demonstrates how Spring WebMVC can be used to
 * handle HTTP GET requests via object-oriented programming.  These
 * requests are mapped to methods that synchronously find all
 * available flights, find the best price for a flight request, get a
 * list of airports, and find departure dates for a given pair of
 * airports.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@code FlightController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 */
@RestController
public class FlightController {
    /**
     * This auto-wired field connects the FlightController to the
     * FlightService.
     */
    @Autowired
    FlightService flightService;

    /**
     * Temporary login end-point for android-client to check that
     * basic auth works.
     */
    @GetMapping("/login")
    public Boolean login() {
        return true;
    }

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @param currency The desired currency
     * @return A {@link List} of matching {@link Flight} objects in
     * the desired currency
     */
    @GetMapping(FLIGHTS)
    public List<Flight> findFlights(@RequestParam String departureAirport,
                                    @RequestParam String departureDate,
                                    @RequestParam String arrivalAirport,
                                    @RequestParam String currency) {
        return flightService
            // Forward to the FlightService.
            .findFlights(departureAirport,
                         LocalDate.parse(departureDate),
                         arrivalAirport,
                         currency);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @param currency The desired currency
     * @return A {@link List} containing {@link Flight} objects
     * matching the {@link FlightRequest} that share the lowest price
     * in the desired currency
     */
    @GetMapping(BEST_PRICE)
    List<Flight> findBestPrice(@RequestParam String departureAirport,
                               @RequestParam String departureDate,
                               @RequestParam String arrivalAirport,
                               @RequestParam String currency) {
        return flightService
            // Forward to the FlightService.
            .findBestPrice(departureAirport,
                           LocalDate.parse(departureDate),
                           arrivalAirport,
                           currency);
    }

    /**
     * Finds all the known airports.
     *
     * @return A {@link List} of {@code Airport} objects
     */
    @GetMapping(AIRPORTS)
    public List<Airport> getAirports() {
        return flightService
            // Forward to the FlightService.
            .getAirports();
    }

    /**
     * Find all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all matching dates
     */
    @GetMapping(FLIGHT_DATES)
    public List<LocalDate> findDepartureDates(@RequestParam String departureAirport,
                                              @RequestParam String arrivalAirport) {
        return flightService
            // Forward to the FlightService.
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}
