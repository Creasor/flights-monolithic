package server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * This class provides the entry point into the monolithic Spring
 * Flight Listing App (FLApp).
 *
 * The {@code SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.
 *
 * The {@code EnableJpaRepositories} annotation enables the use of JPA
 * repositories by scanning the package of the annotated configuration
 * class for Spring Data repositories.
 *
 * The {@code EnableResourceServer} annotation means that this app
 * expects an access token to process requests.
 *
 * The {@code EnableWebSecurity} annotation allows Spring to
 * automatically apply this class to the global WebSecurity.
 */
@SpringBootApplication
@EnableJpaRepositories
@EnableResourceServer
@EnableWebSecurity
public class FlightApplication {
    /**
     * A static main() entry point is needed to run this Spring
     * application.
     */
    public static void main(String[] args) {
        // Launch this application.
        SpringApplication.run(FlightApplication.class, args);
    }
}
