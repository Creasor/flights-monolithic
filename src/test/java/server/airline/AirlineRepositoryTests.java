package server.airline;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Used to isolate and test only the AirlineRepository.
 */
@DataJpaTest(properties = {"spring.datasource.data="})
public class AirlineRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AirlineRepository repository;

    List<Flight> flights;
    List<Flight> expected;
    FlightRequest flightRequest;

    @BeforeEach
    public void beforeEach() {
        entityManager.clear();

        flights = FlightFactory
            .builder()
            .airlines(3)
            .airports(3)
            .dailyFlights(3)
            .from(LocalDate.now())
            .to(LocalDate.now().plusDays(3))
            .currency("USD")
            .build();

        for (Flight flight : flights) {
            entityManager.persist(flight);
        }

        entityManager.flush();

        flightRequest = FlightFactory
            .buildRequestFrom(flights.get(0));

        expected = flights
            .stream()
            .filter(flight -> flightRequest .getDepartureAirport().equals(flight.getDepartureAirport()) 
                    && flightRequest.getDepartureDate().equals(flight.getDepartureDate()) 
                    && flightRequest.getArrivalAirport().equals(flight.getArrivalAirport()))
            .collect(Collectors.toList());
    }

    @Test
    public void testFindByDepartureAirportAndDepartureDateAndArrivalAirport() {
        List<Flight> response = repository
            .findByDepartureAirportAndDepartureDateAndArrivalAirport(flightRequest.getDepartureAirport(),
                                                                     flightRequest.getDepartureDate(),
                                                                     flightRequest.getArrivalAirport());

        assertThat(response).isEqualTo(expected);
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expectedDates = flights
            .stream()
            .filter(flight ->
                    flight.getDepartureAirport().equals(flightRequest.getDepartureAirport()) 
                    && flight.getArrivalAirport().equals(flightRequest.getArrivalAirport()))
            .map(Flight::getDepartureDate)
            .distinct()
            .collect(Collectors.toList());

        List<LocalDate> result = repository
            .findDepartureDates(flightRequest.getDepartureAirport(),
                                flightRequest.getArrivalAirport());

        // Make sure the controller was to return a non-empty list.
        assertThat(result).isEqualTo(expectedDates);
    }
}
