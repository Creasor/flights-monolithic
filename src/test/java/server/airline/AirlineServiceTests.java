package server.airline;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import server.common.model.Flight;
import server.common.model.FlightRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Used to isolate and test only the AirlineService.
 */
@SpringBootTest
public class AirlineServiceTests {
    @Autowired
    AirlineService service;

    @MockBean
    AirlineRepository repository;

    List<Flight> flights;
    List<Flight> expected;
    FlightRequest flightRequest;

    @BeforeEach
    public void beforeEach() {
        flights = FlightFactory
            .builder()
            .dailyFlights(3)
            .airlines(3)
            .airlines(3)
            .build();

        flightRequest = FlightFactory
            .buildRequestFrom(flights.get(0));

        expected = flights
            .stream()
            .filter(flight -> flightRequest.getDepartureAirport().equals(flight.getDepartureAirport()) 
                    && flightRequest.getDepartureDate().equals(flight.getDepartureDate()) 
                    && flightRequest.getArrivalAirport().equals(flight.getArrivalAirport()))

            .collect(Collectors.toList());
    }

    @Test
    public void testFindFlights() {
        when(repository
             .findByDepartureAirportAndDepartureDateAndArrivalAirport(flightRequest.getDepartureAirport(),
                                                                      flightRequest.getDepartureDate(),
                                                                      flightRequest.getArrivalAirport()))
            .thenReturn(expected);

        List<Flight> response = service
            .findFlights(flightRequest.getDepartureAirport(),
                         flightRequest.getDepartureDate(),
                         flightRequest.getArrivalAirport());

        verify(repository, times(1))
            .findByDepartureAirportAndDepartureDateAndArrivalAirport(flightRequest.getDepartureAirport(),
                                                                     flightRequest.getDepartureDate(),
                                                                     flightRequest.getArrivalAirport());

        assertThat(response).isEqualTo(expected);
    }

    @Test
    public void testFindBestPrice() {
        Flight bestPrice1 = expected.get(0).withPrice(1.0);
        Flight bestPrice2 = expected.get(1).withPrice(1.0);
        expected.add(bestPrice1);
        expected.add(bestPrice2);

        List<Flight> bestPrice = List.of(bestPrice1, bestPrice2);

        when(repository
             .findByDepartureAirportAndDepartureDateAndArrivalAirport(flightRequest.getDepartureAirport(),
                                                                      flightRequest.getDepartureDate(),
                                                                      flightRequest.getArrivalAirport()))
            .thenReturn(expected);

        List<Flight> response = service.findBestPrice(flightRequest.getDepartureAirport(),
                                                      flightRequest.getDepartureDate(),
                                                      flightRequest.getArrivalAirport());

        verify(repository, times(1))
            .findByDepartureAirportAndDepartureDateAndArrivalAirport(flightRequest.getDepartureAirport(),
                                                                     flightRequest.getDepartureDate(),
                                                                     flightRequest.getArrivalAirport());

        assertThat(response).isEqualTo(bestPrice);
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expected = List.of(LocalDate.parse("1234-01-01"),
                                           LocalDate.parse("1234-01-02"));

        String departureAirport = "from here";
        String arrivalAirport = "to there";

        when(repository
             .findDepartureDates(departureAirport, arrivalAirport))
            .thenReturn(expected);

        assertThat(service
                   .findDepartureDates(departureAirport, arrivalAirport))
            .isEqualTo(expected);

        verify(repository, times(1))
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}
