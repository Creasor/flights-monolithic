package server.flight;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import server.airline.AirlineRepository;
import server.airline.FlightFactory;
import server.airport.AirportRepository;
import server.common.model.Airport;
import server.common.model.Flight;
import server.common.model.FlightRequest;
import server.security.AuthStatefulSecurityConfig;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static server.common.Constants.EndPoint.*;

/**
 * Used to perform an integration test for FlightRequests.
 */
@SpringBootTest(properties = {"spring.datasource.data="})
@Import(AuthStatefulSecurityConfig.class)
@AutoConfigureMockMvc
@WithMockUser(username = "user0", roles = {"USER"})
public class FlightIntegrationTests {
    List<Flight> flights;
    FlightRequest flightRequest;
    List<Flight> expected;

    @Autowired
    AirlineRepository airlineRepository;

    @Autowired
    AirportRepository airportRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void beforeEach() {
        airlineRepository.deleteAll();

        flights = FlightFactory
            .builder()
            .dailyFlights(3)
            .airlines(3)
            .airlines(3)
            .build();

        flightRequest = FlightFactory.buildRequestFrom(flights.get(0));

        expected = flights.stream()
            .filter(flight -> flightRequest.getDepartureAirport().equals(flight.getDepartureAirport()) 
                    && flightRequest.getDepartureDate().equals(flight.getDepartureDate()) 
                    && flightRequest.getArrivalAirport().equals(flight.getArrivalAirport()))
            .collect(Collectors.toList());

        airlineRepository.saveAll(flights);
    }

    @Test
    public void testFindFlights() throws Exception {
        String jsonResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/" + FLIGHTS)
                     .queryParam("departureAirport", flightRequest.getDepartureAirport())
                     .queryParam("departureDate", dateString(flightRequest.getDepartureDate()))
                     .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                     .queryParam("currency", flightRequest.getCurrency()))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<Flight> result = objectMapper
            .readValue(jsonResult, new TypeReference<>() { });

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testBestPrice() throws Exception {
        Flight bestPrice1 = expected.get(0).withPrice(1.0);
        Flight bestPrice2 = expected.get(1).withPrice(1.0);

        List<Flight> bestPrice = List.of(bestPrice1, bestPrice2);
        airlineRepository.saveAll(bestPrice);

        String jsonResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/" + BEST_PRICE)
                     .queryParam("departureAirport", flightRequest.getDepartureAirport())
                     .queryParam("departureDate", dateString(flightRequest.getDepartureDate()))
                     .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                     .queryParam("currency", flightRequest.getCurrency()))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<Flight> result = objectMapper
            .readValue(jsonResult, new TypeReference<>() { });

        assertThat(result).isEqualTo(bestPrice);
    }

    @Test
    public void testFindFlightDates() throws Exception {
        List<LocalDate> expectedDates = flights
            .stream()
            .filter(flight ->
                    flight.getDepartureAirport().equals(flightRequest.getDepartureAirport()) 
                    && flight.getArrivalAirport() .equals(flightRequest.getArrivalAirport()))
            .map(Flight::getDepartureDate)
            .distinct()
            .collect(Collectors.toList());

        String jsonResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/" + FLIGHT_DATES)
                     .queryParam("departureAirport", flightRequest.getDepartureAirport())
                     .queryParam("arrivalAirport", flightRequest.getArrivalAirport()))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<LocalDate> result = objectMapper
            .readValue(jsonResult, new TypeReference<>() { });

        // Make sure the controller was to return a non-empty list.
        assertThat(result).isEqualTo(expectedDates);
    }

    @Test
    public void testFindAllAirports() throws Exception {
        List<Airport> expected = List
            .of(new Airport("ABC", "ABC NAME"),
                new Airport("DEF", "DEF NAME"),
                new Airport("HIJ", "HIJ NAME"));

        airportRepository.saveAll(expected);

        String jsonResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/" + AIRPORTS))
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        assertThat(jsonResult).isNotNull();

        List<Airport> result = objectMapper
            .readValue(jsonResult, new TypeReference<>() { });

        assertThat(result).isEqualTo(expected);
    }

    private String dateString(LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
