package server.flight;

import org.springframework.security.test.context.support.WithMockUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.List;

import org.springframework.context.annotation.Import;
import server.airline.AirlineService;
import server.airline.FlightFactory;
import server.airport.AirportService;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;
import server.common.model.FlightRequest;
import server.exchange.ExchangeService;
import server.security.AuthStatefulSecurityConfig;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Used to isolate and test only the FlightService.
 */
@SpringBootTest
class FlightServiceTest {
    @MockBean
    AirlineService airlineService;

    @MockBean
    AirportService airportService;

    @MockBean
    ExchangeService exchangeService;

    @Autowired
    FlightService flightService;

    @Test
    void findFlights() {
        List<Flight> expected = FlightFactory
            .builder()
            .build();

        FlightRequest flightRequest = FlightFactory
            .buildRequestFrom(expected.get(0));

        when(airlineService.findFlights(flightRequest.getDepartureAirport(),
                                        flightRequest.getDepartureDate(),
                                        flightRequest.getArrivalAirport()))
            .thenReturn(expected);

        List<Flight> result = flightService
            .findFlights(flightRequest.getDepartureAirport(),
                         flightRequest.getDepartureDate(),
                         flightRequest.getArrivalAirport(),
                         flightRequest.getCurrency());

        assertThat(result).isEqualTo(expected);

        verify(airlineService, times(1))
            .findFlights(flightRequest.getDepartureAirport(),
                         flightRequest.getDepartureDate(),
                         flightRequest.getArrivalAirport());
    }

    @Test
    void findBestPrice() {
        List<Flight> expected = FlightFactory
            .builder()
            .build();
        FlightRequest flightRequest = FlightFactory
            .buildRequestFrom(expected.get(0));

        when(airlineService.findBestPrice(flightRequest.getDepartureAirport(),
                                          flightRequest.getDepartureDate(),
                                          flightRequest.getArrivalAirport()))
            .thenReturn(expected);

        List<Flight> result = flightService
            .findBestPrice(flightRequest.getDepartureAirport(),
                           flightRequest.getDepartureDate(),
                           flightRequest.getArrivalAirport(),
                           flightRequest.getCurrency());

        assertThat(result).isEqualTo(expected);

        verify(airlineService, times(1))
            .findBestPrice(flightRequest.getDepartureAirport(),
                           flightRequest.getDepartureDate(),
                           flightRequest.getArrivalAirport());
    }

    @Test
    void findDepartureDates() {
        List<LocalDate> expected = List
            .of(LocalDate.now(), LocalDate.now());
        String departureAirport = "ABC";
        String arrivalAirport = "DEF";

        when(airlineService.findDepartureDates(departureAirport, arrivalAirport))
            .thenReturn(expected);

        List<LocalDate> result = flightService
            .findDepartureDates(departureAirport, arrivalAirport);

        assertThat(result).isEqualTo(expected);

        verify(airlineService, times(1))
            .findDepartureDates(departureAirport, arrivalAirport);
    }

    @Test
    void getAirports() {
        List<Airport> expected = List
            .of(new Airport("ABC", "ABC name"),
                new Airport("DEF", "DEF name"));

        when(airportService.getAirports()).thenReturn(expected);

        List<Airport> result = flightService
            .getAirports();

        assertThat(result).isEqualTo(expected);

        verify(airportService, times(1)).getAirports();
    }

    @Test
    void getRate() {
        ExchangeRate expected =
            new ExchangeRate("USD", "CAN", 1.28);

        when(exchangeService.getRate(expected.getFromCurrency(), expected.getToCurrency()))
            .thenReturn(expected);

        ExchangeRate result = flightService
            .getRate(expected.getFromCurrency(), expected.getToCurrency());

        assertThat(result).isEqualTo(expected);

        verify(exchangeService, times(1))
            .getRate(expected.getFromCurrency(), expected.getToCurrency());
    }
}
