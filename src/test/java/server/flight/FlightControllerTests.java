package server.flight;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import server.airline.FlightFactory;
import server.common.model.Flight;
import server.common.model.FlightRequest;
import server.security.AuthStatefulSecurityConfig;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static server.common.Constants.EndPoint.*;

/**
 * Used to isolate and test only the FlightController.
 */
@SpringBootTest(properties = {"spring.datasource.data="})
@Import(AuthStatefulSecurityConfig.class)
@AutoConfigureMockMvc
@WithMockUser(username = "user0", roles = {"USER"})
public class FlightControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightService service;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testFindFlights() throws Exception {
        List<Flight> flights = FlightFactory
            .builder().build();

        for (Flight expected : flights) {
            FlightRequest flightRequest = FlightFactory
                .buildRequestFrom(expected);

            List<Flight> expectedList = new ArrayList<>(1);
            expectedList.add(expected);

            when(service.findFlights(flightRequest.getDepartureAirport(),
                                     flightRequest.getDepartureDate(),
                                     flightRequest.getArrivalAirport(),
                                     flightRequest.getCurrency()))
                .thenReturn(expectedList);

            String jsonResult = mockMvc
                .perform(MockMvcRequestBuilders
                         .get("/" + FLIGHTS +
                              "?departureAirport=" + flightRequest.getDepartureAirport() +
                              "&departureDate=" + dateString(flightRequest.getDepartureDate()) +
                              "&arrivalAirport=" + flightRequest.getArrivalAirport() +
                              "&currency=" + flightRequest.getCurrency()))
                //                    MockMvcRequestBuilders
                //                            .get("/" + FLIGHTS)
                //                            .queryParam("departureAirport", flightRequest.getDepartureAirport())
                //                            .queryParam("departureDate", dateString(flightRequest.getDepartureDate()))
                //                            .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                //                            .queryParam("currency", flightRequest.getCurrency()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

            List<Flight> result = objectMapper
                .readValue(jsonResult, new TypeReference<>() {});

            List<Flight> cleanedResult = result.stream()
                .map(flight -> flight.withId(null))
                .collect(Collectors.toList());

            verify(service, times(1))
                .findFlights(flightRequest.getDepartureAirport(),
                             flightRequest.getDepartureDate(),
                             flightRequest.getArrivalAirport(),
                             flightRequest.getCurrency());

            assertThat(cleanedResult).isEqualTo(expectedList);

            clearInvocations(service);
        }
    }

    @Test
    public void testFindBestPrice() throws Exception {
        List<Flight> flights = FlightFactory.builder().build();

        for (Flight expected : flights) {
            FlightRequest flightRequest = FlightFactory
                .buildRequestFrom(expected);

            List<Flight> expectedList = new ArrayList<>(1);
            expectedList.add(expected);

            when(service.findBestPrice(flightRequest.getDepartureAirport(),
                                       flightRequest.getDepartureDate(),
                                       flightRequest.getArrivalAirport(),
                                       flightRequest.getCurrency()))
                .thenReturn(expectedList);

            String jsonResult = mockMvc
                .perform(MockMvcRequestBuilders
                         .get("/" + BEST_PRICE)
                         .queryParam("departureAirport", flightRequest.getDepartureAirport())
                         .queryParam("departureDate", dateString(flightRequest.getDepartureDate()))
                         .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                         .queryParam("currency", flightRequest.getCurrency()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

            List<Flight> result = objectMapper
                .readValue(jsonResult, new TypeReference<>() {});

            List<Flight> cleanedResult = result.stream()
                .map(flight -> flight.withId(null))
                .collect(Collectors.toList());

            verify(service, times(1))
                .findBestPrice(flightRequest.getDepartureAirport(),
                               flightRequest.getDepartureDate(),
                               flightRequest.getArrivalAirport(),
                               flightRequest.getCurrency());

            assertThat(cleanedResult).isEqualTo(expectedList);

            clearInvocations(service);
        }
    }

    @Test
    public void testFindFlightDates() throws Exception {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String from = "from here";
        String to = "to there";

        when(service.findDepartureDates(from, to))
            .thenReturn(expected);

        String jsonResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/" + FLIGHT_DATES)
                     .queryParam("departureAirport", from)
                     .queryParam("arrivalAirport", to))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<LocalDate> result = objectMapper
            .readValue(jsonResult, new TypeReference<>() { });

        verify(service, times(1)).findDepartureDates(from, to);

        assertThat(result).isEqualTo(expected);
    }

    private String dateString(LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
