package server.airport;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import server.common.model.Airport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Used to isolate and test only the AirportService.
 */
@SpringBootTest
public class AirportServiceTests {

    @MockBean
    AirportRepository repository;

    @Autowired
    AirportService airportService;

    @Test
    public void testFindAllAirports() {
        List<Airport> expected = List
            .of(new Airport("ABC", "ABC NAME"),
                new Airport("DEF", "DEF NAME"),
                new Airport("HIJ", "HIJ NAME"));

        when(repository.findAll()).thenReturn(expected);

        List<Airport> result = airportService.getAirports();

        assertThat(result).isEqualTo(expected);
        verify(repository, times(1)).findAll();
    }
}
