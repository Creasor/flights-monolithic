package server.airport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import server.common.model.Airport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Used to isolate and test only the AirportRepository.
 */
@DataJpaTest(properties = {"spring.datasource.data="})
public class AirportRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AirportRepository repository;

    @BeforeEach
    public void beforeEach() {
        entityManager.clear();
    }

    @Test
    public void testFindAllAirports() {
        List<Airport> expected = List
            .of(new Airport("ABC", "ABC NAME"),
                new Airport("DEF", "DEF NAME"),
                new Airport("HIJ", "HIJ NAME"));

        for (Airport airport : expected) {
            entityManager.persist(airport);
        }

        entityManager.flush();

        assertThat(repository.findAll()).isEqualTo(expected);
    }
}
