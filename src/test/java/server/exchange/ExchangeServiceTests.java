package server.exchange;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import server.common.model.ExchangeRate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Used to isolate and test only the ExchangeService.
 */
@SpringBootTest
public class ExchangeServiceTests {
    @Autowired
    ExchangeService exchangeService;

    @MockBean
    ExchangeRepository repository;

    @Test
    @WithMockUser("admin")
    public void testExchangeRate() {
        ExchangeRate expected = new ExchangeRate("DEF", "ABC", 2.0);

        when(repository
             .findByFromCurrencyAndToCurrency(expected.getFromCurrency(),
                                              expected.getToCurrency()))
            .thenReturn(expected);

        ExchangeRate result = exchangeService
            .getRate(expected.getFromCurrency(),
                     expected.getToCurrency());

        assertThat(result).isEqualTo(expected);

        verify(repository)
            .findByFromCurrencyAndToCurrency(expected.getFromCurrency(),
                                             expected.getToCurrency());
    }
}
